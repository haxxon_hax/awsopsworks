Chef::Log.info("Mapping the environment_variables node for Environment...")
node[:deploy].each do |application, deploy|
  deploy[:environment_variables].each do |key, value|
    ENV[key] = value
  end
end
