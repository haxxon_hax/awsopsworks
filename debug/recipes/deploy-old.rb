if (defined?(node[:deploy]['lamp'])).nil?
  if node[:deploy]['lamp'][:environment_variables][:USE_ENVIRONMENT] == 'SANDBOX'
    mount '/srv/www' do
      device '172.31.30.84:/export1'
      fstype 'nfs'
      options 'rw'
      action [:mount,:enable]
    end
  elsif node[:deploy]['lamp'][:environment_variables][:USE_ENVIRONMENT] == 'DEV'
    mount '/srv/www' do
      device '172.31.30.84:/export2'
      fstype 'nfs'
      options 'rw'
      action [:mount,:enable]
    end
  elsif node[:deploy]['lamp'][:environment_variables][:USE_ENVIRONMENT] == 'QA'
    mount '/srv/www' do
      device '172.31.22.140:/export1'
      fstype 'nfs'
      options 'rw'
      action [:mount,:enable]
    end
  elsif node[:deploy]['lamp'][:environment_variables][:USE_ENVIRONMENT] == 'UAT'
    mount '/srv/www' do
      device '172.31.22.140:/export2'
      fstype 'nfs'
      options 'rw'
      action [:mount,:enable]
    end
  elsif node[:deploy]['lamp'][:environment_variables][:USE_ENVIRONMENT] == 'PROD'
    mount '/srv/www' do
      device '172.31.29.244:/export1'
      fstype 'nfs'
      options 'rw'
      action [:mount,:enable]
    end
  end
end
