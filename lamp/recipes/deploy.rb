node[:deploy].each do |app, deploy|
  file File.join(deploy[:deploy_to], 'shared', 'config', 'app_data.yml') do
    content JSON.dump(node[:deploy]['lamp'])
  end
end

#if (defined?(node[:deploy]['lamp'])).nil?
#  node[:deploy].each do |app, deploy|
#    file File.join(deploy[:deploy_to], 'shared', 'config', 'app_data.yml') do
#      content YAML.dump(node[:deploy][environment_variables].to_hash)
#    end
#  end
#else
#  node[:deploy].each do |app, deploy|
#    if deploy['lamp'][:environment_variables][:USE_ENVIRONMENT] == 'DEV'
#      template '/tmp/environment.txt' do
#        source 'dev.txt'
#        group 'root'
#        owner 'root'
#        mode '0755'
#      end
#    elsif deploy['lamp'][:environment_variables][:USE_ENVIRONMENT] == 'PROD'
#      template '/tmp/environment.txt' do
#        source 'prod.txt'
#        group 'root'
#        owner 'root'
#        mode '0755'
#      end
#    end
#  end
#end
