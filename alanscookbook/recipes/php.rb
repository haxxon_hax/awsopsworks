template '/etc/php.ini' do
  source 'php.ini.erb'
  group 'root'
  owner 'root'
  mode '0644'
end
