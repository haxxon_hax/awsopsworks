service 'rpcbind' do
  action [:enable, :start]
  not_if "chkconfig --list rpcbind | grep -qx 'on'"
end

service 'nfs' do
  action [:enable, :start]
  not_if "chkconfig --list nfs | grep -qx 'on'"
end

template '/tmp/create_nfs_exports.sh' do
  source 'create_nfs_exports.sh'
  group 'root'
  owner 'root'
  mode '0755'
end

execute '/tmp/create_nfs_exports.sh' do
  user 'root'
  command '/tmp/create_nfs_exports.sh'
end
