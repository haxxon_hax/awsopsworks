#!/bin/sh

EXPORT_LIST=`/bin/df -h | /bin/grep '/export[0-9]' | /bin/awk  '{print $6}'`

for i in $EXPORT_LIST; do
   /bin/echo "$i	172.31.0.0/16(rw,no_root_squash)" >> /etc/exports
done

/usr/sbin/exportfs -a
